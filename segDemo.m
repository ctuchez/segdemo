%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%
SHOW_DETAIL = true;

% Domain dimensions
NUM_ROWS = 20;
NUM_COLS = NUM_ROWS; % square grid

% Population parameters
PROB_EMPTY = 0.5; % probability that a cell is empty
POP_RATIO = 2; % population bias: ratio of group "A" ("+1") size to "B" ("-1") size

% Maximum number of time steps
MIN_DIM = min ([NUM_ROWS NUM_COLS]);
MAX_TIME = round (MIN_DIM * log (MIN_DIM));

%%%%%%%%%%%%%
% Main Demo %
%%%%%%%%%%%%%

% Initial grid
X_0 = createDomain (NUM_ROWS, NUM_COLS, PROB_EMPTY, POP_RATIO);

% Draw initial grid
figure (1); clf;
n_A = length (find (X_0 > 0)); % # in group "A" ("+1")
n_B = length (find (X_0 < 0)); % # in group "B" ("-1")
showDomain (X_0, sprintf ('Initial [%d:%d]', n_A, n_B));
pause

% Main simulation (time-stepping) loop
X_t = X_0;
for t=1:MAX_TIME,

  display (sprintf ('=== Time step: %d ===', t))

  % Measure the "color" of each neighborhood
  C_t = measureColor (X_t);

  % Determine who is unhappy with their neighborhood
  M_t = (X_t ~= 0) & ((X_t .* C_t) <= 0);

  % Move the "A" ("+1") group first
  A_t = (X_t > 0); % Group "A" ("+1")
  F_A_t = getFree (X_t) .* (C_t >= 0); % desirable locations
  Y_t = moveGroup (X_t, A_t .* M_t, F_A_t);

  % Move the "B" ("-1") group next
  B_t = -(X_t < 0); % Group "B" ("-1")
  F_B_t = getFree (Y_t) .* (C_t <= 0); % desirable locations
  Z_t = moveGroup (Y_t, B_t .* M_t, F_B_t);

  n_moved = sum (sum (abs (abs (Z_t) - abs (X_t)))) / 2;
  display (sprintf ('  %d cells were movable.', sum (sum (M_t))));
  display (sprintf ('  %d cells moved.', n_moved));

  figure (1); clf;
  if SHOW_DETAIL,
    subplot (2, 2, 1); showDomain (X_t, sprintf ('Population: t=%d', t));
    n_A = length (find (C_t == 1)); n_B = length (find (C_t == -1));
    subplot (2, 2, 2); showDomain (C_t, sprintf ('Neighborhood color [%d:%d]', n_A, n_B));
    n_M = length (find (M_t ~= 0));
    subplot (2, 2, 3); showDomain (M_t, sprintf ('Movable [%d]', n_M));
    subplot (2, 2, 4); showDomain (Z_t, sprintf ('New population: t+1=%d', t+1));
  else
    n_A = length (find (X_0 == 1)); n_B = length (find (X_0 == -1));
    subplot (1, 2, 1); showDomain (X_0, sprintf ('Initial [%d:%d]: t=0', n_A, n_B));
    n_A = length (find (Z_t == 1)); n_B = length (find (Z_t == -1));
    subplot (1, 2, 2); showDomain (Z_t, sprintf ('Current population [%d:%d]: t=%d', n_A, n_B, t));
  end

  pause;
  X_t = Z_t;

  if n_moved == 0,
    display ('*** END: Reached steady state. ***')
    break;
  end
end

% eof
