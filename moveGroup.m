function Y = moveGroup (X, G, F)
% moveGroup  Randomly moves a subset of matrix elements.
%
%   Y = moveGroup (X, G, F)
%
% Given:
%   X(1:m, 1:n) = a matrix of {-1, 0, +1} values
%   G(1:m, 1:n) = a mask of values to try to move
%   F(1:m, 1:n) = a mask of potential destinations
%
% For each G(i,j) == 1, this routine tries to move the value X(i,j) to
% some position (k, l) such that F(k,l) == 1. The position (k, l) is
% chosen uniformly at random from among all such positions as given by
% F. Returns a new matrix Y(1:m, 1:n) with both the "unmoved" and
% "moved" values.
  
  [m, n] = size (X);
  assert (all (size (F) == [m n]))

  % Determine who is movable
  [I_movable, J_movable, G_movable] = find (G);
  n_movable = length (I_movable);
  K_movable = randperm (n_movable);

  [I_free, J_free] = find (F);
  n_free = length (I_free); % no. of free locations
  K_free = randperm (n_free);

  % Determine who is moving and to where
  n_moving = min ([n_movable n_free]);
  K_moving = K_movable(1:n_moving);
  K_dest = K_free(1:n_moving);

  % Move 'em!
  Move_outs = sparse (I_movable(K_moving), J_movable(K_moving), G_movable(K_moving), m, n);
  Move_ins = sparse (I_free(K_dest), J_free(K_dest), G_movable(K_moving), m, n);
  Y = X - Move_outs + Move_ins;
end

% eof
